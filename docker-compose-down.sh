#!/bin/bash

echo "start compose down nginx-proxy"
cd nginx-proxy/
docker-compose down
cd ..
echo "compose down for nginx-proxy have finish"

sleep 3

echo "start compose down app-staging"
cd app-staging/
docker-compose down
cd ..
echo "compose down for app-staging have finish"

sleep 3

echo "start compose down app-production"
cd app-production/
docker-compose down
cd ..
echo "compose down for app-production have finish"




