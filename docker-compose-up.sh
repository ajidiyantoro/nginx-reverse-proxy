#!/bin/bash

echo "start compose up app-staging"
cd app-staging/
docker-compose up -d
cd ..
echo "compose up for app-staging have finish"

sleep 3

echo "start compose up app-production"
cd app-production/
docker-compose up -d
cd ..
echo "compose up for app-production have finish"

sleep 3

echo "start compose up nginx-proxy"
cd nginx-proxy/
docker-compose  up -d
cd ..
echo "compose up for nginx-proxy have finish"
